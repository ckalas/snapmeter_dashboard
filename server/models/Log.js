var mongoose = require('mongoose')

const Schema = mongoose.Schema;


const readingSchema = new Schema({
    timestamp: Date,
    lat: Number,
    lon: Number,
    value: Number
})

const logSchema = new Schema({
  // You may need to add other fields like owner
  nodeID: String,
  messageID: String,
  reading: readingSchema
},
{collection: 'logs'});

const Log = mongoose.model('Log', logSchema);
module.exports = Log;