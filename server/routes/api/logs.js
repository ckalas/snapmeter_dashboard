const Log = require('../../models/Log');

module.exports = (app) => {
  /*
  app.get('/api/logs', (req, res, next) => {
    Log.find({})
      .exec()
      .then((log) => res.json(log))
      .catch((err) => next(err));
    });*/
    app.get('/api/details/*', (req,res,next) => {
        Log.find({nodeID: req.params['0']})
        .exec()
        .then((log) => res.json(log))
        .catch((err)=> next(err));
    });

    app.get('/api/logs/devices', (req,res,next) => {
        Log.aggregate(
            [
                //{ $match: { "reading.lon": {$ne:-1}}},
                {
                    $group : 
                        { 
                            _id: {"nodeID": "$nodeID"}
    
                        }
                },
                
                {
                $project:
                    {
                        "_id" : 0,
                        "nodeID": "$_id.nodeID",
    
                }
                }
            ]
    ) 
    .exec()
    .then((log) => res.json(log))
    .catch((err) => next(err));
  });
  
  app.get('/api/logs', (req, res, next) => {
      Log.aggregate(
        [
            { $sort: { "reading.timestamp": -1}},
            { $match: { "reading.lon" : {"$ne": -1}}},
            {
              $group :
                    {
                        _id: "$nodeID",
                        features: {$push: "$$ROOT"}
                    }
            },
            
            {
                $replaceRoot :
                    {
                        newRoot: {$arrayElemAt: ["$features", 0]}
                    }
            },
            
            {
                $project:
                    {
                        "_id" : 0,
                        "nodeID": "$nodeID",
                        "lat": "$reading.lat",
                        "lon": "$reading.lon",
                        "value": "$reading.value"
                    }
            }
            
            
         
        ]
      )
      .exec()
      .then((log) => res.json(log))
      .catch((err) => next(err));
    });



    app.get('/api/logs/latest_each', (req, res, next) => { 
      Log.aggregate(
        [
            { $sort: { "reading.timestamp": -1}},
            {
              $group :
                    {
                        _id: "$nodeID",
                        latestValue: {$first: "$reading.value"},
                        timestamp: {$first: "$reading.timestamp"}
                    }
            }
          
        ]
      ) 
      .exec()
        .then((log) => res.json(log))
        .catch((err) => next(err));
      });
  
}
