const Log = require('../../models/Location');

module.exports = (app) => {

    app.get('/api/locations/', (req,res,next) => {
        Log.find({category: 'node'})
        .exec()
        .then((log) => res.json(log))
        .catch((err)=> next(err));
    });

  
}
