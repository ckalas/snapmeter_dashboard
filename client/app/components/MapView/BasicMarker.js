import React from "react";
import { Redirect } from 'react-router';
import _ from "lodash"


export default class BasicMarker extends React.Component {


    // todo: onclick takes you to info page
    onClick (props) {
        
        if (this.props.length === 1) {
            this.setState({redirect: true, selection: props.id});
        }
    }

    componentWillMount() {
        this.setState({redirect: false})
    }

    render() {

        console.log('state', this.state)
        if (this.state.redirect) {
            console.log(this.state)
            return <Redirect push to={"/detail/"+this.state.selection} />;
        }

        console.log(this.props.id)
        // calculate color from level
        // get value on mouse over?

        const val = _.mean(this.props.value)

        var colors = []

        if (val > 160) {
            colors = ['rgba(0, 240, 0, 1)','rgba(0, 180, 0, 0.3)']
        }
        else if (val > 80) {
            colors = ['rgba(240, 240, 0, 1)','rgba(180, 180, 0, 0.3)']
        }

        else {
            colors = ['rgba(240, 0, 0, 1)','rgba(180, 0, 0, 0.3)']
        }


        return (
            <svg  class='marker-icon' width='140' height='150' transform="translate(-70,-150)" xmlns="http://www.w3.org/2000/svg" version="1.1">
                <g class='marker' >
                <polygon class='outline'  points="37,105 70,145 103,105" fill="white" onClick={this.onClick.bind(this, this.props)}/>
                    <circle class='outline' cx="50%" cy="50%" r="45" fill='white' onClick={this.onClick.bind(this, this.props)}/>
                    <circle  cx="50%" cy="50%" r="40" fill={colors[1]} onClick={this.onClick.bind(this, this.props)}/>
                    <circle cx="50%" cy="50%" r="30" fill={colors[0]} onClick={this.onClick.bind(this, this.props)}/>
                    <text x="50%" y="55%" textAnchor="middle" stroke="black" fontFamily="'Lucida Grande', sans-serif" 
                          fontSize="30" onClick={this.onClick.bind(this, this.props)}>{this.props.length}</text>
                    
                </g>
            </svg>
        ) 

        
    }
}