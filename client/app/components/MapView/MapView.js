import React from "react";
import ReactDOM from "react-dom";
import GoogleMap from 'google-map-react'

import Context from "../Home/Context"
import BasicMarker from "./BasicMarker"

import _ from "lodash"


function degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
}

function distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
    const earthRadiusKm = 6371;

    var dLat = degreesToRadians(lat2-lat1);
    var dLon = degreesToRadians(lon2-lon1);

    lat1 = degreesToRadians(lat1);
    lat2 = degreesToRadians(lat2);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    return earthRadiusKm * c;
}

function cluster(points, diagonal) {

    var i = points.length
    var groups = []
    var group = []
    //console.log('POINTS', points)

    while (i--) {
        var p1 = points[i]

        if (!p1.lat) {
            continue
        }

        //console.log('startloop', i, points, points.length)

        var clone = [ ...points ]
        group = []

        console.log(p1)
        clone.forEach((p2) => {
            
            if (!p1 || !p2) {
                groups.push([p2])
                points.splice(points.findIndex(x => x===p2), 1);
                return;
            }
            var dist = distanceInKmBetweenEarthCoordinates(p1.lat,p1.lon, p2.lat,p2.lon)
            console.log('DIST', p1, p2, diagonal/dist)
            if (p1===p2 || diagonal/dist > 10) { 
                //console.log('deleting', p2.nodeID)
                group.push(p2)
                points.splice(points.findIndex(x => x===p2), 1);
                
            }

        });


        if (group.length > 0) {
            //console.log('pushing', group)
            groups.push(group) 
        } 
        
    }
    //console.log('returning', groups)
    return groups
}

export default class MapView extends React.Component {

    

    constructor(props, context) {
        super(props);
        console.log('data in child is: ', props)
        this.state = {
            clusters: [],
            latestReadings: props.value,
            ignore: false
        }
    }


    onChange(params) {

        const nw = params.marginBounds.nw
        const se = params.marginBounds.se
        const ne = params.marginBounds.ne
        //console.log(params)
        const diagonal = distanceInKmBetweenEarthCoordinates(ne.lat, ne.lng, se.lat, se.lng)
        //console.log(diagonal)
        this.setState({ 
                clusters: cluster(this.props.value.slice(), diagonal)
        })

        if (this.state.clusters.length < 1 && !this.state.ignore) {
            if (window.confirm('No valid GPS data, redirect to detail view?'))
            {
                document.location = '/detail/' + this.state.latestReadings[0].nodeID
               
            }
            else
            {

            }
            this.setState({ignore: true})
        }
    }


    render() {

        const divStyle = {
            height: '65vh',
            width: '100vw',
            overflow: 'auto'
        }
        console.log(this.state.clusters)
        return (
            <Context.Consumer>
                {(value) => (
                    <div class='map-div' style={divStyle}>
                        
                        <GoogleMap
                            bootstrapURLKeys={{ key: "AIzaSyANiAToD8sjKsqcxGTTz3aVkPUFyju1XDk" }}   
                            center={ {lat: 31.299, lng: 120.5853} }
                            zoom={8}
                            onChange={this.onChange.bind(this)}>
                            {this.state.clusters.map(c => 
                                <BasicMarker
                                    //bootstrapURLKeys={{ key: "AIzaSyANiAToD8sjKsqcxGTTz3aVkPUFyju1XDk" }}
                                    key={c[0].lat.toString() + c[0].lon.toString()}
                                    length={c.length}
                                    value={c.map(c=>c.value)}
                                    lat={_.mean(c.map(c=>c.lat))}
                                    lng={_.mean(c.map(c=>c.lon))}
                                    id={c.map(c=>c.nodeID)}/>)}
                        </GoogleMap>
                    </div>

                )}
            </Context.Consumer>
        );
    }
}