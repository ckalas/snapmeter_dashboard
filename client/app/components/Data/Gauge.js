import React from 'react';
import Snap from 'snapsvg-cjs';

function polar_to_cartesian (cx, cy, radius, angle) {
    var radians;
    radians = (angle - 90) * Math.PI / 180.0;
    return [Math.round((cx + (radius * Math.cos(radians))) * 100) / 100, Math.round((cy + (radius * Math.sin(radians))) * 100) / 100];
  };
  
function svg_circle_arc_path(x, y, radius, start_angle, end_angle) {
    var end_xy, start_xy;
    start_xy = polar_to_cartesian(x, y, radius, end_angle);
    end_xy = polar_to_cartesian(x, y, radius, start_angle);
    return "M " + start_xy[0] + " " + start_xy[1] + " A " + radius + " " + radius + " 0 0 0 " + end_xy[0] + " " + end_xy[1];
  };

function animate_arc(ratio, svg, perc) {
    var arc, center, radius, startx, starty;
    arc = svg.path('');
    center = 500;
    radius = 450;
    startx = 0;
    starty = 450;
    
    return Snap.animate(0, ratio, (function(val) {
      var path;
      arc.remove();
      path = svg_circle_arc_path(500, 500, 450, -90, val * 180.0 - 90);
      arc = svg.path(path)
      arc.attr({
        style: "stroke: " + String(makeColor(ratio))
        

      });
    }), Math.round(1000 * ratio), mina.easeinout);
  };

function intToHex(i) {
    var hex = parseInt(i).toString(16);
    return (hex.length < 2) ? "0" + hex : hex;
}   

function makeColor(value) {
    // value must be between [0, 510]
    value = Math.min(Math.max(0,value), 1) * 510;

    var redValue;
    var greenValue;
    if (value < 255) {
        redValue = 255;
        greenValue = Math.sqrt(value) * 16;
        greenValue = Math.round(greenValue);
    } else {
        greenValue = 255;
        value = value - 255;
        redValue = 256 - (value * value / 255)
        redValue = Math.round(redValue);
    }
    
    return "#" + intToHex(redValue) + intToHex(greenValue) + "00";
}

// todo: pass fill level and calculate label/ratio

export default class Gauge extends React.Component {

    svgRender() {
        var ratio, svg, perc;
        console.log(this.props)
        ratio = this.props.value/100
        svg = Snap(this.svgDiv);
        console.log(svg)
        animate_arc(ratio, svg);
      }
      componentDidMount() {
        this.svgRender();
      }
      componentDidUpdate() {
        this.svgRender();
      }

      render() {
        

        return ( 
            <div class='gauge'>
                <svg viewBox="0 0 1000 500" ref={d=>this.svgDiv=d}>
                    <path d="M 950 500 A 450 450 0 0 0 50 500" />
                    <text class='percentage' textAnchor="middle" alignmentBaseline="middle" x="500" y="430" fontSize="140" fontWeight="bold">{this.props.value.toFixed(2) + '%'}</text>
                </svg>
            </div>
            )
      }
}
