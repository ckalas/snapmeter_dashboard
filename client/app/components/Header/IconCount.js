import React from 'react';
import gauge from './gauge.svg'

const IconCount = (props) => (
  <span class='icon-count'>
            <img class='icon' src={props.icon}/>
            <span class='icon-number'> {props.count} </span> <span class='icon-type'> {props.type} </span>
  </span>
  );


export default IconCount;