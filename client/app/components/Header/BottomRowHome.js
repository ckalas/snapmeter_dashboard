import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'
import warning from './warning.svg'
import rightarrow from './rightarrow.svg'

const BottomRowHome = () => (
    <Grid class='headerBottom'>
        <Col xs={6}>
        <span class='attention-div'>
        <img class='bottom-row-home-icons' src={warning} />
        <span class='warning-number'>6</span> <span class='bottom-row-home-string'> Packs need your attention </span>
        </span>
        </Col>
        <Col xs={6}>
        <span class='arrow-div'>
        <span class='bottom-row-home-string'> Watch issues </span>
        <img class='bottom-row-home-icons right-arrow' src={rightarrow}/>
        </span>
        </Col>
     </Grid>
);

export default BottomRowHome;
