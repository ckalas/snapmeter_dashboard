import React, { Component } from 'react';
import rightarrow from './rightarrow.svg'
import leftarrow from './leftarrow.svg'
import map from './map.svg'
import {Grid, Row, Col} from 'react-bootstrap'
import { Redirect } from 'react-router';

// todo: link forward and back to cycle packs
export default class BottomRowDetail extends Component {


    constructor(props) {
        super(props);
        const current = props.props.location.pathname.split('/')[2]
        this.state = {redirect: false, next: '', currentID: current}
    }

    onClick(direction) {
        
        if (this.state.data) {

            const devices = _.map(this.state.data, "nodeID")
        
            var nextDevice = (devices.indexOf(this.state.currentID) + direction) % devices.length
            if (nextDevice < 0 ) {
                nextDevice += devices.length
            }
            //this.setState({redirect: true, next: nextDevice})
            return  "/detail/"+devices[nextDevice]
            //console.log('clicky click', direction, devices, devices.indexOf(this.state.currentID))
        }
    }


    componentDidMount() {
        var data;
        fetch('/api/logs/devices', {method: 'GET'})
        .then(res => res.json())
        .then(json => {
          console.log('devices', json)
          this.setState({data: json})
        });
    
    
    }
    render() {

            return (
                <Grid class='headerBottom'>
                    <Row>
                        <Col xs={4}>
                        <span class='attention-div'>
                        <a href={this.onClick(-1)}>
                        <img src={leftarrow} width={80} height={80} />
                        </a>
                        </span>
                        </Col>

                        <Col xs={4}>
                        <span class='home-btn'>
                        <a class='no-underline' href="/">
                            <img class='home' src={map} width={70} height={70}/>
                        </a>
                        </span>
                        </Col>

                        <Col xs={4}>
                        <span class='arrow-div'>
                        <a href={this.onClick(1)}>
                        <img class='right-arrow' src={rightarrow} width={80} height={80}/>
                        </a>
                        </span>
                        </Col>
                    </Row>
                </Grid>
        )
        
    }

}


