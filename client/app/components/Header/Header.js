import React from 'react';

import { Link } from 'react-router-dom';
import {Navbar, NavItem, Nav, Glyphicon, Grid, Row} from 'react-bootstrap'

import IconCount from './IconCount'
import BottomRowHome from './BottomRowHome'

import gauge from './gauge.svg'
import marker from './marker.svg'
import building from './building.svg'

import warning from './warning.svg'
import rightarrow from './rightarrow.svg'


import Gauge from "../Data/Gauge"
import BottomRowDetail from './BottomRowDetail';

import logo from "../../../public/assets/img/logo.png"

// todo: pass icons as props through context
// todo: add watch issues 
// todo: make  icons clickable

export default class Header extends React.Component {

  constructor(props) {
    super(props);
  
    console.log(this.props)
  }
  
  render() {
      const ICONS = [{file: building, name: 'COMPANIES', count:74},
                     {file: marker, name: 'SITES', count: 199},
                     {file: gauge, name: 'PACKS', count: 3982}]

      console.log('props', this.props.match)
      return (
        <div>
          <Navbar>
            <Navbar.Header>
              <Navbar.Text>
                <span class='bold'>SNAP</span><span class='nobold'>METER</span>
                </Navbar.Text>
            </Navbar.Header>
            <Navbar.Toggle />
            <Navbar.Collapse>
              <Nav pullRight>
                <NavItem eventKey={1} href="#">
                  <span class='glyphicon glyphicon-user'></span>
                </NavItem>
                <NavItem eventKey={2} href="#">
                  <span class='glyphicon glyphicon-star'></span>
                </NavItem>
                <NavItem eventKey={3} href="#">
                  <span class='glyphicon glyphicon-bell'></span>
                </NavItem>
                <NavItem eventKey={4} href="#">
                  <span class='glyphicon glyphicon-cog'></span>
                </NavItem>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          
          <div class='headerMiddle'>
          <Grid>
          {ICONS.map(c => 
                <IconCount icon={c.file} type={c.name} count={c.count}/>)}
          </Grid>
          </div>
          
          
          {this.props.location.pathname === '/' ? <BottomRowHome /> : <BottomRowDetail props={this.props}/>}
  
        </div>
      ) 

      
  }
}
