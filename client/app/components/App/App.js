import React, { Component } from 'react';
import {withRouter} from 'react-router'

import Header from '../Header/Header';

const WrappedHeader = withRouter(Header)

const App = ({ children }) => (
  <>
    <WrappedHeader />
    <main >
      {children}
    </main>

  </>
);

export default App;
