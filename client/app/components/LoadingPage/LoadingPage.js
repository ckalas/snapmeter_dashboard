import React, { Component } from 'react';
import { FadeLoader} from 'react-spinners';
import {Grid, Row, Col} from 'react-bootstrap'



export default class LoadingPage extends Component {

  render() {
  
      
      return ( 
            <div class='loading-div'>
                    <FadeLoader color={'#51aee1'} loading={true} />
            </div>
       )
    }

}
