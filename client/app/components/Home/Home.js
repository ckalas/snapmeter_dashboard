import React, { Component } from 'react';
import MapView from "../MapView/MapView"
import Context from "./Context"
import 'whatwg-fetch';
import Gauge from "../Data/Gauge"
import { BarLoader} from 'react-spinners';
import LoadingPage from '../LoadingPage/LoadingPage'
class Provider extends Component {

  render() {
    return (
      <Context.Provider value={this.props.value}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

class Home extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      data: []
    };


  }

  componentDidMount() {
      fetch('/api/logs', {method: 'GET'})
      .then(res => res.json())
      .then(json => {
        console.log('did mount', json)
        this.setState({data: json})
  
      });
  }

  render() {
  
    if (this.state.data.length > 0) {
      console.log('data is: ', this.state.data)
      return (
        <Provider value={this.state.data}>
          <MapView value={this.state.data}/>
        </Provider>
      );
    }
    else {
      return ( <LoadingPage />)
    }
  }
}

export default Home;
