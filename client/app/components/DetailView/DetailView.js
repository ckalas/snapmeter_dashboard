import React, { Component } from 'react';
import { SyncLoader} from 'react-spinners';
import {Line, Bar, defaults} from 'react-chartjs-2'
import {Grid, Row, Col, ButtonGroup, Button} from 'react-bootstrap'
import Context from "../Home/Context"
import 'whatwg-fetch';
import Gauge from "../Data/Gauge"
import _ from 'lodash'

import StatusSummary from './StatusSummary'
import LoadingPage from '../LoadingPage/LoadingPage'


const MAX_VALUE = 200
const FULL = 0.8
const EMPTY = 0.2


defaults.global.legend.position = 'bottom'
defaults.global.legend.padding = 20;
defaults.global.maintainAspectRatio= false
//defaults.global.scale.barWidth = 1.0

export default class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
        data: [],
        nodeID: null,
        };
    }

    // todo: grab the data for a single cylinder
    // todo: maybe get related stuff too
    componentDidMount() {

        // get data for selected node

        const nodeID = this.props.match.params.path
        fetch('/api/details/' + nodeID, {method: 'GET'})
        .then(res => res.json())
        .then(json => {
          console.log('did mount', json)
          const data = json
          const latest = _.last(data)
          const zoom = data.length
          this.setState({   
            data: json,
            nodeID: nodeID,
            latest: latest,
            percentage: (latest.reading.value/MAX_VALUE) * 100,
            zoom: data.length,
            times: data.map(x => new Date(x.reading.timestamp).toDateString()),
            dates: data.map(x => new Date(x.reading.timestamp)),    
            values: data.map(x => x.reading.value)})
        });

        // grab a list of all nodes to allow cycling through?

    }

    getStatus(value) {
        if(value >= FULL) return 'Full';
        if(value <= EMPTY) return 'Empty';
        return 'Normal' 
    }

    getTimeSinceLastReading() {
        const now = new Date()
        var lastReading = _.clone(_.last(this.state.dates))
        lastReading.setHours(lastReading.getHours() - 8)
        console.log(lastReading, now)
        var diffMs = (now - lastReading); // milliseconds between now & Christmas
        var diffDays = Math.floor(diffMs / 86400000); // days
        var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes

        return({'days': diffDays, 'hours': diffHrs, 'minutes': diffMins})
    }

    // todo: catch the time to avoid infinite loop xd
    setLastData(time) {
        var startIndex = -1;
        const data = this.state.data
        const times = data.map(x => new Date(x.reading.timestamp).toDateString())
        if (time > 0) {

        
        var date = new Date(this.state.latest.reading.timestamp)
        var hours = Math.abs(date - new Date(this.state.data[0].reading.timestamp)) / 36e5;
        console.log(time,hours, time > hours)
        date.setHours(date.getHours() - time)
        startIndex = times.indexOf(date.toDateString())
        if (time > hours) {
            startIndex = 0
        }

        console.log(date)
        var accumulatedTime = 0
        while (startIndex < 0) {
            
            if (accumulatedTime > hours) {
                startIndex = 0
                break
            }
            accumulatedTime += 24
            date.setHours(date.getHours() - 24)
            startIndex = this.state.times.indexOf(date.toDateString())
            console.log(date.toDateString())
        } 

        console.log(startIndex)

        }
        else {
            startIndex = 0;
        }
        

        this.setState((prevState, props) => ({
            times: data.map(x => new Date(x.reading.timestamp).toDateString()).slice(startIndex, data.length),   
            values: data.map(x => x.reading.value).slice(startIndex,data.length)
        }));
        
    }

    // TODO: calculate time scale and whether you show date or time
    // TODO: add button to move between cylinders in detail view
    render() {
        if (this.state.data.length > 0) {
            

        var dataFormatted = {
            // Labels should be Date objects
            labels: this.state.times,
            datasets: [{
                borderWidth: 1,
                fill: false,
                borderColor: '#07658A',
                backgroundColor:'#07658A',
                label: 'Angle',
                data: this.state.values,
            }]
        }

        console.log(this.state)

        return(
            <div class='detail-view'>
            <Grid class='detail-view'>
                <Row>
                <Col xs={6} class='text-center'>
                    <div class='gauge-div'>
                    <Gauge value={this.state.percentage} />
                    </div>

                    <ButtonGroup class='graph-buttons'>
                        <Button onClick={this.setLastData.bind(this,23)} >Last Day</Button>
                        <Button onClick={this.setLastData.bind(this,167)}>Last Week</Button>
                        <Button onClick={this.setLastData.bind(this,719)}>Last Month</Button>
                        <Button onClick={this.setLastData.bind(this,-1)}>All Data</Button>
                    </ButtonGroup>
                    <div class='graph'>
                        <Bar
                            height={300}
                            data={dataFormatted}
                        
                        />
                    </div>

                </Col>

                <Col xs={6}>
                    <Row class='detail-row'>
                        <span class='status-large'> Node ID: </span> <span class='status-large light-blue'> {this.state.nodeID} </span>
                    </Row>
                    <Row>
                        <StatusSummary status={this.getStatus(this.state.percentage/100)} 
                                       charge={'lol as if i can predict'}
                                       lastReading={this.getTimeSinceLastReading()}
                        />
                    </Row>
                    <Row>
                        Site info
                    </Row>
                    <Row>
                        Related packs / near packs
                    </Row>
                </Col>
                </Row>
            </Grid>
            </div>
        )
    }
    else {
        return (<LoadingPage />)
    }
    }
}

