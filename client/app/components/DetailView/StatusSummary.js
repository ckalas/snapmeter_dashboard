import React from 'react';

import {Grid, Row, Col} from 'react-bootstrap'

// todo: decide on how to get site - just find nearest site to gps position?
// todo: find related packs - by site
// todo: let the time scale be set hours/days/minutes

const StatusSummary = (props) => (
    <table class='status-summary'>
        <tbody>
        <tr>
            <th>
                <div class='status-large'>
                    {props.status}
                </div>
                <div class='status-small'>
                    Status
                </div>
            </th>


            <th>
                <div class='status-large'>
                   Coming Soon!
                </div>
                <div class='status-small'>
                   Estimated Capacity
                </div>
            </th>

            <th>
                <div class='status-large'>
                    {props.lastReading['hours'] + props.lastReading['days']*24 + ' hours'}
                </div>
                <div class='status-small'>
                    Last Reading
                </div>
            </th>
        </tr>
        </tbody>
     </table>
);

export default StatusSummary;
